# Duniter Protocol Crypto Python

DUP-crypto (Duniter protocol cryptography) binding from pure Rust to Python3.

## Install

	pip3 install --user --upgrade dup_crypto

## Build & install

	sudo pip3 install --upgrade setuptools wheel
	sh build_wheel.sh
	pip3 install --user --upgrade target/wheel/dist/dup_crypto-*-py3-none-any.whl

No need to install any additional crypto package!
