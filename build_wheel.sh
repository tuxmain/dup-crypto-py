cargo build --release || exit
strip -s target/release/libdup_crypto_py.so

mkdir -p target/wheel/src/dup_crypto || exit

cp py/setup.py target/wheel/ || exit
cp py/__init__.py target/wheel/src/dup_crypto/ || exit
cp py/__test__.py target/wheel/src/dup_crypto/ || exit
cp LICENSE target/wheel/ || exit
cp README.md target/wheel/ || exit
cp target/release/libdup_crypto_py.so target/wheel/src/dup_crypto/libdup_crypto.so || exit

cd target/wheel || exit

python3 setup.py sdist bdist_wheel
