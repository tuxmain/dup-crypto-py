mod dewif;
mod keys;
mod mnemonic;
mod private_message;
mod seeds;

use pyo3::prelude::*;

#[pymodule]
fn libdup_crypto(py: Python<'_>, m: &PyModule) -> PyResult<()> {
	let m_dewif = PyModule::new(py, "dewif")?;
	dewif::init_module(m_dewif)?;
	m.add_submodule(m_dewif)?;

	let m_keys = PyModule::new(py, "keys")?;
	keys::init_module(py, m_keys)?;
	m.add_submodule(m_keys)?;

	let m_mnemonic = PyModule::new(py, "mnemonic")?;
	mnemonic::init_module(m_mnemonic)?;
	m.add_submodule(m_mnemonic)?;

	let m_private_message = PyModule::new(py, "private_message")?;
	private_message::init_module(m_private_message)?;
	m.add_submodule(m_private_message)?;

	let m_seeds = PyModule::new(py, "seeds")?;
	seeds::init_module(m_seeds)?;
	m.add_submodule(m_seeds)
}
