pub mod ed25519;

use pyo3::prelude::*;

pub fn init_module(py: Python<'_>, m: &PyModule) -> PyResult<()> {
	let m_ed25519 = PyModule::new(py, "ed25519")?;
	ed25519::init_module(m_ed25519)?;
	m.add_submodule(m_ed25519)
}
