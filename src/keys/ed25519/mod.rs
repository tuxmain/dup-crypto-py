use crate::seeds::Seed32;

use dup_crypto::bases::b58::ToBase58;
use dup_crypto::keys::{KeyPair, PublicKey, Signator as _, Signature};
use pyo3::{exceptions::*, prelude::*, types::*};
use std::convert::{TryFrom, TryInto};

#[pyclass]
#[text_signature = "(bytes)"]
#[derive(Clone)]
pub struct Pubkey {
	pub inner: dup_crypto::keys::ed25519::PublicKey,
}

#[pymethods]
impl Pubkey {
	#[new]
	fn new(raw: &[u8]) -> PyResult<Self> {
		match dup_crypto::keys::ed25519::PublicKey::try_from(raw) {
			Ok(v) => Ok(Self { inner: v }),
			Err(_) => Err(PyValueError::new_err(format!(
				"Pubkey size must be {} bytes",
				dup_crypto::keys::ed25519::PUBKEY_SIZE_IN_BYTES
			))),
		}
	}

	#[staticmethod]
	#[text_signature = "(str)"]
	fn from_base58(raw: &str) -> PyResult<Self> {
		match dup_crypto::keys::ed25519::PublicKey::from_base58_with_checksum_opt(raw) {
			Ok(v) => Ok(Self { inner: v }),
			Err(e) => Err(PyValueError::new_err(format!("{:?}", e))),
		}
	}

	#[text_signature = "($self)"]
	fn to_base58(&self) -> String {
		self.inner.to_base58()
	}

	#[text_signature = "($self)"]
	fn checksum(&self) -> String {
		self.inner.checksum()
	}

	#[text_signature = "($self, msg, signature, /)"]
	fn verify(&self, msg: &[u8], signature: &[u8]) -> PyResult<bool> {
		match signature.try_into() {
			Ok(signature) => match self
				.inner
				.verify(msg, &dup_crypto::keys::ed25519::Signature(signature))
			{
				Ok(_) => Ok(true),
				Err(e) => Err(PyValueError::new_err(format!("{:?}", e))),
			},
			Err(_) => Err(PyValueError::new_err(format!(
				"Sig size must be {} bytes",
				dup_crypto::keys::ed25519::SIG_SIZE_IN_BYTES
			))),
		}
	}

	#[text_signature = "($self)"]
	fn __bytes__(&self) -> PyObject {
		let gil = Python::acquire_gil();
		let py = gil.python();
		PyBytes::new(py, self.inner.as_ref()).to_object(py)
	}

	#[text_signature = "($self, other, /)"]
	fn __eq__(&self, other: &Self) -> bool {
		self.inner == other.inner
	}
}

#[pyclass]
struct Signator {
	inner: dup_crypto::keys::ed25519::Signator,
}

#[pymethods]
impl Signator {
	#[getter]
	fn pubkey(&self) -> Pubkey {
		Pubkey {
			inner: self.inner.public_key(),
		}
	}

	#[text_signature = "($self, bytes, /)"]
	fn sign(&self, msg: &[u8]) -> PyObject {
		let gil = Python::acquire_gil();
		let py = gil.python();
		PyBytes::new(py, &self.inner.sign(msg).to_bytes_vector()).to_object(py)
	}
}

#[pyclass]
#[derive(Clone)]
pub struct Keypair {
	pub inner: dup_crypto::keys::ed25519::Ed25519KeyPair,
}

#[pymethods]
impl Keypair {
	#[getter]
	fn pubkey(&self) -> Pubkey {
		Pubkey {
			inner: self.inner.public_key(),
		}
	}

	#[staticmethod]
	#[text_signature = "(Seed32)"]
	fn from_seed32(seed: Seed32) -> Self {
		Self {
			inner: dup_crypto::keys::ed25519::KeyPairFromSeed32Generator::generate(seed.inner),
		}
	}

	#[staticmethod]
	#[args(log_n = "12", r = "16", p = "1")]
	#[text_signature = "(salt, password, log_n=12, r=16, p=1, /)"]
	fn from_scrypt(salt: String, psw: String, log_n: u8, r: u32, p: u32) -> Self {
		Self {
			inner: dup_crypto::keys::ed25519::KeyPairFromSaltedPasswordGenerator::with_parameters(
				log_n, r, p,
			)
			.generate(dup_crypto::keys::ed25519::SaltedPassword::new(salt, psw)),
		}
	}

	#[staticmethod]
	#[text_signature = "()"]
	fn random() -> PyResult<Self> {
		match dup_crypto::keys::ed25519::Ed25519KeyPair::generate_random() {
			Ok(v) => Ok(Self { inner: v }),
			Err(e) => Err(PyException::new_err(format!("{:?}", e))),
		}
	}

	#[text_signature = "($self)"]
	fn signator(&self) -> Signator {
		Signator {
			inner: self.inner.generate_signator(),
		}
	}

	#[text_signature = "($self, msg, signature, /)"]
	fn verify(&self, msg: &[u8], signature: &[u8]) -> PyResult<bool> {
		match signature.try_into() {
			Ok(signature) => match self
				.inner
				.verify(msg, &dup_crypto::keys::ed25519::Signature(signature))
			{
				Ok(_) => Ok(true),
				Err(e) => Err(PyValueError::new_err(format!("{:?}", e))),
			},
			Err(_) => Err(PyValueError::new_err(format!(
				"Sig size must be {} bytes",
				dup_crypto::keys::ed25519::SIG_SIZE_IN_BYTES
			))),
		}
	}
}

pub fn init_module(m: &PyModule) -> PyResult<()> {
	m.add_class::<Keypair>()?;
	m.add_class::<Pubkey>()?;
	m.add_class::<Signator>()?;
	m.add("SIG_SIZE", dup_crypto::keys::ed25519::SIG_SIZE_IN_BYTES)?;
	m.add(
		"PUBKEY_SIZE",
		dup_crypto::keys::ed25519::PUBKEY_SIZE_IN_BYTES,
	)
}
