use dup_crypto::bases::b58::ToBase58;
use pyo3::{exceptions::*, prelude::*, types::*};
use std::convert::TryInto;

#[pyclass]
#[text_signature = "(bytes)"]
#[derive(Clone)]
pub struct Seed32 {
	pub inner: dup_crypto::seeds::Seed32,
}

#[pymethods]
impl Seed32 {
	#[new]
	fn new(raw: &[u8]) -> PyResult<Self> {
		match raw.try_into() {
			Ok(v) => Ok(Self {
				inner: dup_crypto::seeds::Seed32::new(v),
			}),
			Err(_) => Err(PyValueError::new_err(format!(
				"Seed size must be {} bytes",
				dup_crypto::seeds::SEED_32_SIZE_IN_BYTES
			))),
		}
	}

	#[staticmethod]
	#[text_signature = "()"]
	fn random() -> PyResult<Self> {
		match dup_crypto::seeds::Seed32::random() {
			Ok(v) => Ok(Self { inner: v }),
			Err(e) => Err(PyException::new_err(format!("{:?}", e))),
		}
	}

	#[staticmethod]
	#[text_signature = "(str)"]
	fn from_base58(raw: &str) -> PyResult<Self> {
		match dup_crypto::seeds::Seed32::from_base58(raw) {
			Ok(v) => Ok(Self { inner: v }),
			Err(e) => Err(PyValueError::new_err(format!("{:?}", e))),
		}
	}

	#[staticmethod]
	#[args(log_n = "12", r = "16", p = "1")]
	#[text_signature = "(salt, password, log_n=12, r=16, p=1, /)"]
	fn from_scrypt(salt: &[u8], psw: &[u8], log_n: u8, r: u32, p: u32) -> Self {
		Self {
			inner: dup_crypto::keys::ed25519::KeyPairFromSaltedPasswordGenerator::with_parameters(
				log_n, r, p,
			)
			.generate_seed(psw, salt),
		}
	}

	#[text_signature = "($self)"]
	fn to_base58(&self) -> String {
		self.inner.to_base58()
	}

	#[text_signature = "($self)"]
	fn __bytes__(&self) -> PyObject {
		let gil = Python::acquire_gil();
		let py = gil.python();
		PyBytes::new(py, self.inner.as_ref()).to_object(py)
	}

	#[text_signature = "($self, other, /)"]
	fn __eq__(&self, other: &Self) -> bool {
		self.inner == other.inner
	}
}

pub fn init_module(m: &PyModule) -> PyResult<()> {
	m.add_class::<Seed32>()?;
	m.add("SEED_SIZE", dup_crypto::seeds::SEED_32_SIZE_IN_BYTES)
}
