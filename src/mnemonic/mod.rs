use crate::seeds::Seed32;

use pyo3::{exceptions::*, prelude::*, types::*};

fn language_from_str(lang: &str) -> Option<dup_crypto::mnemonic::Language> {
	Some(match lang {
		"en" => dup_crypto::mnemonic::Language::English,
		"es" => dup_crypto::mnemonic::Language::Spanish,
		"fr" => dup_crypto::mnemonic::Language::French,
		"it" => dup_crypto::mnemonic::Language::Italian,
		"ja" => dup_crypto::mnemonic::Language::Japanese,
		"ko" => dup_crypto::mnemonic::Language::Korean,
		"zh_HANS" => dup_crypto::mnemonic::Language::ChineseSimplified,
		"zh_HANT" => dup_crypto::mnemonic::Language::ChineseTraditional,
		_ => return None,
	})
}

#[pyclass]
#[text_signature = "(n_words, language)"]
struct Mnemonic {
	inner: dup_crypto::mnemonic::Mnemonic,
}

#[pymethods]
impl Mnemonic {
	#[new]
	fn new(n_words: usize, lang: &str) -> PyResult<Self> {
		Ok(Self {
			inner: dup_crypto::mnemonic::Mnemonic::new(
				dup_crypto::mnemonic::MnemonicType::for_word_count(n_words)
					.map_err(|_| PyValueError::new_err("Invalid word count"))?,
				language_from_str(lang).ok_or_else(|| PyValueError::new_err("Unknown language"))?,
			)
			.map_err(|e| PyException::new_err(format!("{:?}", e)))?,
		})
	}

	#[staticmethod]
	#[text_signature = "(bytes, language)"]
	fn from_entropy(entropy: &[u8], lang: &str) -> PyResult<Self> {
		Ok(Self {
			inner: dup_crypto::mnemonic::Mnemonic::from_entropy(
				entropy,
				language_from_str(lang).ok_or_else(|| PyValueError::new_err("Unknown language"))?,
			)
			.map_err(|e| PyException::new_err(format!("{:?}", e)))?,
		})
	}

	#[staticmethod]
	#[text_signature = "(phrase, language)"]
	fn from_phrase(phrase: &str, lang: &str) -> PyResult<Self> {
		Ok(Self {
			inner: dup_crypto::mnemonic::Mnemonic::from_phrase(
				phrase,
				language_from_str(lang).ok_or_else(|| PyValueError::new_err("Unknown language"))?,
			)
			.map_err(|e| PyException::new_err(format!("{:?}", e)))?,
		})
	}

	#[getter]
	fn entropy(&self) -> PyObject {
		let gil = Python::acquire_gil();
		let py = gil.python();
		PyBytes::new(py, self.inner.entropy()).to_object(py)
	}

	#[getter]
	fn phrase(&self) -> &str {
		self.inner.phrase()
	}

	#[text_signature = "($self)"]
	fn seed(&self) -> Seed32 {
		Seed32 {
			inner: dup_crypto::mnemonic::mnemonic_to_seed(&self.inner),
		}
	}

	#[staticmethod]
	#[text_signature = "(phrase, language)"]
	fn validate(phrase: &str, lang: &str) -> PyResult<bool> {
		dup_crypto::mnemonic::Mnemonic::validate(
			phrase,
			language_from_str(lang).ok_or_else(|| PyValueError::new_err("Unknown language"))?,
		)
		.map_err(|e| PyException::new_err(format!("{:?}", e)))?;
		Ok(true)
	}
}

pub fn init_module(m: &PyModule) -> PyResult<()> {
	m.add(
		"LANGUAGES",
		vec!["en", "es", "fr", "it", "ja", "ko", "zh_HANS", "zh_HANT"],
	)?;
	m.add_class::<Mnemonic>()
}
