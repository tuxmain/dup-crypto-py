use crate::keys::ed25519::{Keypair, Pubkey};

use dup_crypto::keys::Signature;
use pyo3::{exceptions::*, prelude::*, types::*};

const ALGO_CHACHA20POLY1305: u32 = 1;
const ALGO_AES256GCM: u32 = 2;

/// Decrypts a private message
///
/// Arguments:
///   aad: bytes (additionally authenticated data)
///   algo: uint_32
///   msg: bytes
///   keypair: Keypair
///
/// Returns: (msg: bytes, pubkey: Pubkey, signature: bytes?)
///
/// `aad` is unencrypted data in the message.
#[pyfunction]
#[text_signature = "(aad, algo, msg, keypair, /)"]
fn decrypt(
	aad: &[u8],
	algo: u32,
	mut msg: Vec<u8>,
	keypair: Keypair,
) -> PyResult<(PyObject, Pubkey, Option<PyObject>)> {
	match dup_crypto::private_message::decrypt_private_message(
		dup_crypto::private_message::Aad::from(aad),
		match algo {
			ALGO_CHACHA20POLY1305 => dup_crypto::private_message::Algorithm::Chacha20Poly1305,
			ALGO_AES256GCM => dup_crypto::private_message::Algorithm::Aes256Gcm,
			_ => return Err(PyValueError::new_err("Unknown algo")),
		},
		&mut msg,
		&keypair.inner,
	) {
		Ok(v) => {
			let gil = Python::acquire_gil();
			let py = gil.python();
			Ok((
				PyBytes::new(py, v.message).to_object(py),
				Pubkey {
					inner: v.sender_public_key,
				},
				v.signature_opt
					.map(|o| PyBytes::new(py, &o.to_bytes_vector()).to_object(py)),
			))
		}
		Err(e) => Err(PyValueError::new_err(format!("{:?}", e))),
	}
}

/// Encrypts a private message
///
/// Arguments:
///   aad: bytes (additionally authenticated data)
///   algo: uint_32
///   sign: bool
///   msg: bytes
///   receiver: Pubkey
///   sender: Keypair
///
/// Returns: bytes
///
/// If `sign` is True, the message will be signed inside the encrypted message. The signature will be verifiable by anyone if the receiver publishes it. If `sign` is False, only the receiver will be able to verify the signature.
///
/// `aad` is unencrypted data in the message.
#[pyfunction]
#[text_signature = "(aad, algo, sign, msg, receiver, sender, /)"]
fn encrypt(
	aad: &[u8],
	algo: u32,
	sign: bool,
	mut msg: Vec<u8>,
	receiver: Pubkey,
	sender: Keypair,
) -> PyResult<PyObject> {
	dup_crypto::private_message::encrypt_private_message(
		dup_crypto::private_message::Aad::from(aad),
		match algo {
			ALGO_CHACHA20POLY1305 => dup_crypto::private_message::Algorithm::Chacha20Poly1305,
			ALGO_AES256GCM => dup_crypto::private_message::Algorithm::Aes256Gcm,
			_ => return Err(PyValueError::new_err("Unknown algo")),
		},
		if sign {
			dup_crypto::private_message::AuthenticationPolicy::Signature
		} else {
			dup_crypto::private_message::AuthenticationPolicy::PrivateAuthentication
		},
		&mut msg,
		&receiver.inner,
		&sender.inner,
	)
	.map_err(|e| PyValueError::new_err(format!("{:?}", e)))?;

	let gil = Python::acquire_gil();
	let py = gil.python();
	Ok(PyBytes::new(py, &msg).to_object(py))
}

pub fn init_module(m: &PyModule) -> PyResult<()> {
	m.add_function(pyo3::wrap_pyfunction!(decrypt, m)?)?;
	m.add_function(pyo3::wrap_pyfunction!(encrypt, m)?)?;
	m.add("ALGO_CHACHA20POLY1305", ALGO_CHACHA20POLY1305)?;
	m.add("ALGO_AES256GCM", ALGO_AES256GCM)
}
