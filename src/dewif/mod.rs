use crate::keys::ed25519::Keypair;

use pyo3::{exceptions::*, prelude::*};

/// Reads the content of a DEWIF file
///
/// Arguments:
///   content: str
///   passphrase: str
///   expected_currency: uint_32? (None if any currency is accepted)
///
/// Returns: [dup_crypto.keys.ed25519.Keypair]
#[pyfunction(expected_currency = "None")]
#[text_signature = "(content, passphrase, expected_currency=None, /)"]
fn read_dewif(
	content: &str,
	passphrase: &str,
	expected_currency: Option<u32>,
) -> PyResult<Vec<Keypair>> {
	match dup_crypto::dewif::read_dewif_file_content(
		match expected_currency {
			Some(currency) => dup_crypto::dewif::ExpectedCurrency::Specific(
				dup_crypto::dewif::Currency::from(currency),
			),
			None => dup_crypto::dewif::ExpectedCurrency::Any,
		},
		content,
		passphrase,
	) {
		Ok(v) => Ok(v
			.filter_map(|o| {
				if let dup_crypto::keys::KeyPairEnum::Ed25519(v) = o {
					Some(Keypair { inner: v })
				} else {
					None
				}
			})
			.collect()),
		Err(e) => Err(PyException::new_err(format!("{:?}", e))),
	}
}

/// Generates DEWIF file content
///
/// Arguments:
///   currency: uint_32
///   keypair: dup_crypto.ed25519.Keypair
///   log_n: uint_8
///   passphrase: str
///
/// Returns: str
#[pyfunction]
#[text_signature = "(currency, keypair, log_n, passphrase, /)"]
fn write_dewif(currency: u32, keypair: Keypair, log_n: u8, passphrase: String) -> String {
	dup_crypto::dewif::write_dewif_v3_content(
		dup_crypto::dewif::Currency::from(currency),
		&keypair.inner,
		log_n,
		&passphrase,
	)
}

/// Change DEWIF file passphrase
///
/// Arguments:
///   content: str
///   old_passphrase: str
///   new_passphrase: str
///
/// Returns: str
#[pyfunction]
#[text_signature = "(content, old_passphrase, new_passphrase, /)"]
fn change_dewif_passphrase(
	content: &str,
	old_passphrase: &str,
	new_passphrase: &str,
) -> PyResult<String> {
	dup_crypto::dewif::change_dewif_passphrase(content, old_passphrase, new_passphrase)
		.map_err(|e| PyException::new_err(format!("{:?}", e)))
}

pub fn init_module(m: &PyModule) -> PyResult<()> {
	m.add_function(pyo3::wrap_pyfunction!(read_dewif, m)?)?;
	m.add_function(pyo3::wrap_pyfunction!(write_dewif, m)?)?;
	m.add_function(pyo3::wrap_pyfunction!(change_dewif_passphrase, m)?)?;
	m.add("CURRENCY_G1", dup_crypto::dewif::G1_CURRENCY)?;
	m.add("CURRENCY_G1_TEST", dup_crypto::dewif::G1_TEST_CURRENCY)
}
